# testbench

> C++ programming exercise ([Artin C++ testbench](https://testbench.artin.cz))

## Assignments

### getClosestToZero

```c++
#include <vector>
// Hint: you can use modern C++11 features in every example
/**
  * Please implement this method to
  * return the number in the array that is closest to zero.
  * If there are two equally close to zero elements like 2 and -2
  * - consider the positive element to be "closer" to zero.
  */
int getClosestToZero(std::vector<int>& arr) {
}
```
### countChunks

```c++
#include <vector>
/**
  * Please implement this method to return count of chunks in given array.
  *
  * Chunk is defined as continous sequence of one or more numbers separated by one or more zeroes.
  * Array can contain leading and trailing zeroes.
  *
  * Example: array [5, 4, 0, 0, -1, 0, 2, 0, 0] contains 3 chunks
  */
int countChunks(std::vector<int>& arr) {
}
```
### getLevelSum

```c++
#include <vector>
// Please follow this interface
class Node {
public:
    int getValue();
    std::vector<Node>& getChildren();
};
/**
  * Please implement this method to
  * traverse the tree and return the sum of the values (Node.getValue()) of all nodes
  * at the level N in the tree.
  * Node root is assumed to be at the level 0. All its children are level 1, etc.
  */
int getLevelSum(Node& root, int n) {
}
```
### getReversalsToSort

```c++
#include <vector>
/**
  * You need to sort an array of integers by repeatedly reversing
  * the order of the first several elements of it.
  *
  * For example, to sort [11,14,12,13], you need to  reverse the order of the first two (2)
  * elements and get [14,11,12,13], then reverse the order of the first four (4) elements
  * and get [13,12,11,14] and then reverse the order of the first three (3) elements.
  *
  * The method should return array of integers corresponding to the required reversals.
  * For the previous example, given an array [11,14,12,13]
  * the method should return a array with integers [2,4,3].
  *
  */
std::vector<int> getReversalsToSort(std::vector<int>& arr) {
}
```
