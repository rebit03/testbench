#ifndef _H_A6474886_3BB4_4B1A_A8B5_4699963F53D0_
#define _H_A6474886_3BB4_4B1A_A8B5_4699963F53D0_

#include <vector>

namespace testbench {

	class Node {
		int m_value{ 0 };
		std::vector<Node> m_children;
	public:
		Node(int value)
			: m_value(value)
		{}
		Node(int value, std::initializer_list<Node> children)
			: m_value(value)
			, m_children(children)
		{}

		int getValue() { return m_value; }
		std::vector<Node>& getChildren() { return m_children; }
	};

	int getClosestToZero(std::vector<int>& arr);
	int countChunks(std::vector<int>& arr);
	int getLevelSum(Node& root, int n);
	std::vector<int> getReversalsToSort(std::vector<int>& arr);

}

#endif // _H_A6474886_3BB4_4B1A_A8B5_4699963F53D0_
