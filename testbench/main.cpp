#include "stdafx.h"
#include "testbench.h"

#include <iostream>
#include <stdint.h>

int32_t main(int32_t argc, char* argv[])
{
	using namespace std;
	using namespace testbench;

	{
		vector<int> arr{ -5, -10, -2, 2, 4 };
		int result{ getClosestToZero(arr) };
		cout << "Closest to zero [-5, -10, -2, 2, 4]: " << result << endl;
	}
	cout << endl;
	{
		vector<int> arr{ 0, 0, 5, 4, 0, 0, -1, 0, 2, 0, 0 };
		int result{ countChunks(arr) };
		cout << "Chunks count [0, 0, 5, 4, 0, 0, -1, 0, 2, 0, 0]: " << result << endl;
	}
	cout << endl;
	{
		vector<int> arr{ 0, 0, 0, 0 };
		int result{ countChunks(arr) };
		cout << "Chunks count [0, 0, 0, 0]: " << result << endl;
	}
	cout << endl;
	{
		Node root{ 10, {
			{ 1, { 1, 1 } },
			{ 2, { 2, 2 } },
			{ 3, { 3, 3 } } }
		};
		cout << "{ 10, {" << endl;
		cout << "\t{ 1, { 1, 1 } }," << endl;
		cout << "\t{ 2, { 2, 2 } }," << endl;
		cout << "\t{ 3, { 3, 3 } } }" << endl;
		cout << "}" << endl;
		for (int level{ 0 }; level < 3; ++level) {
			int sum{ getLevelSum(root, level) };
			cout << "Sum at level " << level << ": " << sum << endl;
		}
	}
	cout << endl;
	{
		vector<int> arr{ 11, 14, 12, 13 };
		auto reversals{ getReversalsToSort(arr) };
		cout << "[11, 14, 12, 13] reversals [ ";
		for (auto i : reversals) {
			cout << i << " ";
		}
		cout << "]" << endl;
	}
	cout << endl;
	{
		vector<int> arr{ 2, 11, 14, -3, 12, -2, 13, 1, 0, 20, 10 };
		auto reversals{ getReversalsToSort(arr) };
		cout << "[2, 11, 14, -3, 12, -2, 13, 1, 0, 20, 10] reversals [ ";
		for (auto i : reversals) {
			cout << i << " ";
		}
		cout << "]" << endl;
	}

	return 0;
}
