#include "stdafx.h"
#include "testbench.h"

#include <algorithm>
#include <iterator>
#include <cmath>

using namespace std;

namespace testbench {

	int getClosestToZero(vector<int>& arr)
	{
		if (arr.empty()) {
			return 0;
		}

		int closest{ arr.front() };
		for (const int& num : arr) {
			if (0 == num) {
				return 0;
			}

			const int absClosest{ abs(closest) };
			const int absNum{ abs(num) };

			if (absClosest == absNum) {
				closest = max(closest, num);
			}
			else if (absNum < absClosest) {
				closest = num;
			}
		}
		return closest;
	}

	int countChunks(vector<int>& arr)
	{
		if (arr.empty()) {
			return 0;
		}

		int chunks{ 0 };

		auto end{ arr.cend() };
		auto it{ arr.cbegin() };
		while (true) {
			while (it != end && 0 == *it)
			{	// consume zeroes
				it++;
			}
			if (it != end)
			{	// if not at the end of the list it's a chunk
				chunks++;
			}
			else
			{	// done
				break;
			}
			while (it != end && 0 != *it)
			{	// consume chunk
				it++;
			}
		};

		return chunks;
	}

	int getLevelSum(Node& root, int n)
	{
		if (0 == n) {
			return root.getValue();
		}
		else {
			n--;
			int sum{ 0 };
			auto& children{ root.getChildren() };
			for (auto& child : children) {
				sum += getLevelSum(child, n);
			}
			return sum;
		}
	}

	vector<int> getReversalsToSort(vector<int>& arr)
	{
		vector<int> reversals;

		if (arr.empty() || 1 == arr.size()) {
			return reversals;
		}

		// pancake sort
		for (auto end{ arr.end() }; end != arr.begin(); --end) {
			auto it{ max_element(arr.begin(), end) };
			it++; // reverse() is 'end' exclusive
			if (it != end)
			{	// if max. element is not already at the end of the list
				if ((it - 1) != arr.begin())
				{	// put max. element to the front
					reversals.push_back(static_cast<int>(distance(arr.begin(), it)));
					reverse(arr.begin(), it);
				}
				// flip
				reversals.push_back(static_cast<int>(distance(arr.begin(), end)));
				reverse(arr.begin(), end);
			}
		}

		return reversals;
	}

}
